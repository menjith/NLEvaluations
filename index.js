const scrollContainer = document.querySelector('.scroll-container');

scrollContainer.addEventListener('mousedown', (e) => {
    const startX = e.pageX + scrollContainer.scrollLeft;
    let dragging = true;

    const onMouseMove = (e) => {
        if (!dragging) return;
        const x = startX - e.pageX;
        scrollContainer.scrollLeft = x;
    };

    const onMouseUp = () => {
        dragging = false;
        scrollContainer.removeEventListener('mousemove', onMouseMove);
        scrollContainer.removeEventListener('mouseup', onMouseUp);
    };

    scrollContainer.addEventListener('mousemove', onMouseMove);
    scrollContainer.addEventListener('mouseup', onMouseUp);
});

let arr=Array(30).fill().map((_, i) => "<div class='item'>"+ (i+1) +"</div>")
scrollContainer.innerHTML=arr.join("")
let classarr= [".free",".start",".contact"]

document.querySelectorAll(".item").forEach(item=>item.addEventListener("click",(e)=>{
  let value=e.target.innerHTML

  classarr.forEach(className=>document.querySelector(className).classList.remove("btn-primary"))
  const ele= document.querySelector(".curr-clicked")
  if(ele){ele.classList.remove("curr-clicked");ele.classList.remove("bg-success")}


  e.target.classList.add("curr-clicked")
  e.target.classList.add("bg-success")

  if(value<11)document.querySelector(".free").classList.add("btn-primary")
  else if(value<21)document.querySelector(".start").classList.add("btn-primary")
  else document.querySelector(".contact").classList.add("btn-primary")

  document.querySelector("#order").value=value
}))



const container = document.querySelector('.custom-container');
const itemsContainer = document.querySelector('.custom-items');
const loader = document.querySelector('.custom-loader');
const spinner = document.querySelector('.custom-loader-spinner');

let isLoading = false;
let currentPage = 1;

const fetchData = async () => {
    isLoading = true;
    spinner.style.display = 'block';

    // Simulate fetching data from an API (replace with your actual data fetching logic)
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${currentPage}&_limit=10`);
        const data = await response.json();

        data.forEach(itemData => {
            const item = document.createElement('div');
            item.classList.add('custom-item');
            item.innerHTML = `
                <h3>${itemData.title}</h3>
                <p>${itemData.body}</p>
            `;
            itemsContainer.appendChild(item);
        });

        currentPage++;
    } catch (error) {
        console.error('Error fetching data:', error);
    } finally {
        isLoading = false;
        spinner.style.display = 'none';
    }
};

const options = {
    root: null,
    rootMargin: '0px',
    threshold: 1, // Trigger when 10% of the item is visible
};

const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
        console.log("outside",entry)
        if (entry.isIntersecting && !isLoading) {
            fetchData();
            console.log(entry)
        }
    });
}, options);

observer.observe(loader);

// Initial data load

